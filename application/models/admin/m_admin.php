<?php



defined('BASEPATH') or exit('No direct script access allowed');

class m_admin extends CI_Model
{
    public function show_Admin()
    {
        $query = $this->db->query("SELECT * FROM admin");
        return $query->result_array();
    }

    public function show_Mahasiswa()
    {
        $query = $this->db->query("SELECT * FROM mahasiswa");
        return $query->result_array();
    }

    public function show_CommentIG()
    {
        $query = $this->db->query("SELECT * FROM tb_instagram_acc");
        return $query->result_array();
    }

    public function show_CommentFB()
    {
        $query = $this->db->query("SELECT * FROM tb_facebook_acc");
        return $query->result_array();
    }

    public function show_CommentTwitter()
    {
        $query = $this->db->query("SELECT * FROM tb_twitter_acc");
        return $query->result_array();
    }


    //update query mulai dari sini 

    public function update_admin($data, $id)
    {

        $this->db->where('id', $id);
        $this->db->update('admin', $data);
        return TRUE;
    }


    public function update_comment_ig($data, $id)
    {

        $this->db->where('id_data_ig', $id);
        $this->db->update('tb_instagram_acc', $data);
        return TRUE;
    }

    public function update_comment_fb($data, $id)
    {

        $this->db->where('id_data_fb', $id);
        $this->db->update('tb_facebook_acc', $data);
        return TRUE;
    }

    public function update_comment_tw($data, $id)
    {

        $this->db->where('id_data_twitter', $id);
        $this->db->update('tb_twitter_acc', $data);
        return TRUE;
    }

    public function update_Mhs($data, $id)
    {

        $this->db->where('id_mhs', $id);
        $this->db->update('mahasiswa', $data);
        return TRUE;
    }


    // delete query mulai dari sini
    public function deleteAdmin($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('admin');
        return TRUE;
    }


    public function deleteMhs($id)
    {
        $this->db->where('id_mhs', $id);
        $this->db->delete('mahasiswa');
        return TRUE;
    }

    public function deleteCommentIG($id)
    {
        $this->db->where('id_data_ig', $id);
        $this->db->delete('tb_instagram_acc');
        return TRUE;
    }

    public function deleteCommentFB($id)
    {
        $this->db->where('id_data_fb', $id);
        $this->db->delete('tb_facebook_acc');
        return TRUE;
    }

    public function deleteCommentTwitter($id)
    {
        $this->db->where('id_data_twitter', $id);
        $this->db->delete('tb_twitter_acc');
        return TRUE;
    }


    // create query mulai dari sini
    public function createAdmin($data)
    {
        $this->db->insert('admin', $data);
        return TRUE;
    }

    public function createMhs($data)
    {
        $this->db->insert('mahasiswa', $data);
        return TRUE;
    }
    
    public function createCommentIG($data)
    {
        $this->db->insert('tb_instagram_acc', $data);
        return TRUE;
    }

    public function createCommentFB($data)
    {
        $this->db->insert('tb_facebook_acc', $data);
        return TRUE;
    }

    public function createCommentTwitter($data)
    {
        $this->db->insert('tb_twitter_acc', $data);
        return TRUE;
    }

    

    // count untuk dashboard disini querynya
    
    public function hitungJumlahAssetIG()
    {
        $query = $this->db->get('tb_instagram_acc');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function hitungJumlahAssetFB()
    {
        $query = $this->db->get('tb_facebook_acc');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    public function hitungJumlahAssetTwitter()
    {
        $query = $this->db->get('tb_twitter_acc');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

    

    public function hitungJumlahAssetMhs()
    {
        $query = $this->db->get('mahasiswa');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }


    public function hitungJumlahAssetAdmin()
    {
        $query = $this->db->get('admin');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }


    //query cadangan

    // public function pencarianFakultas($data)
    // {

    //     $this->db->where("tingkat", $data);
    //     // return $this->db->get("aps");
    // }


    // query buat export ke excel

    // public function getAllInstagram()
    //  {
    //       $this->db->select('*');
    //       $this->db->from('tb_instagram_acc');

    //       return $this->db->get();
    //  }

    function fetch_dataIG(){

        $this->db->order_by("id", "DESC");

        $query = $this->db->get("import");

        return $query->result();
    }

    public function getAllInstagram() {
        $this->db->select('*');
        $this->db->from('tb_instagram_acc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllFacebook() {
        $this->db->select('*');
        $this->db->from('tb_facebook_acc');
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllTwitter() {
        $this->db->select('*');
        $this->db->from('tb_twitter_acc');
        $query = $this->db->get();
        return $query->result();
    }


    // tampilkan log disini
    public function show_log()
    {
        $query = $this->db->query("SELECT * FROM tabel_log");
        return $query->result_array();
    }

}
