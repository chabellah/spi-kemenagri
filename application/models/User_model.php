<?php


defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{


    var $user = 'user';


    function checkLoginAdmin($username, $password)
    {
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get($this->user, 1);

        if ($query->num_rows() == 1) {
            return $query->row_array();
        }
    }
}

/* End of file ModelName.php */
