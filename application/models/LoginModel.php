<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LoginModel extends CI_Model
{

    var $table = 'admin';
    var $table2 = 'dosen';
    var $table3 = 'mahasiswa';


    function __construct()
    {
        parent::__construct();
    }

    function checkLoginAdmin($username, $password)
    {
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get($this->table, 1);

        if ($query->num_rows() == 1) {
            return $query->row_array();
        }
    }

    function checkLoginDosen($username, $password)
    {
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get($this->table2, 1);

        if ($query->num_rows() == 1) {
            return $query->row_array();
        }
    }


    function checkLoginMahasiswa($username, $password)
    {

        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get($this->table3, 1);

        if ($query->num_rows() == 1) {
            return $query->row_array();
        }
    }
}
