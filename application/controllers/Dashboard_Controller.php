<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_Controller extends CI_Controller
{

    public function index()
    {
        $data['title'] = 'Sistem Pendataan';
        $this->load->view('view_pendftaran', $data);
    }
}   
