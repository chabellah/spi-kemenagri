<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">UHAMKA AKADEMIK</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
        </div>
        <ul class="sidebar-menu">

            <li class="menu-header">Home</li>
            <li class="nav-item <?php echo (current_url() == base_url() . 'Mahasiswa/index') ? 'active' : '' ?>">
                <a href="<?php echo base_url('Mahasiswa/index') ?>" class=" nav-item <?php echo (current_url() == base_url() . 'Mahasiswa/index') ? 'active' : '' ?>"><i class="fas fa-home-lg"></i><span>Home</span></a>

            </li>



            <li class="menu-header">Matakuliah Management</li>
            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'mahasiswa/tampildatadataMkmhs') ? 'active' : ''  ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"> <i class="fas fa-books"></i>
                    <span>Data Matakuliah</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == 'mahasiswa/tampildataMkmhs') ? "active" : "" ?>"><a href="<?php echo base_url('mahasiswa/tampildataMkmhs') ?>">Lihat Matakuliah</a></li>
                    <!-- <li><a class="nav-link" href="layout-transparent.html">Transparent Sidebar</a></li>
                    <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li> -->
                </ul>
            </li>

            <li class="menu-header">Mahasiswa Management</li>
            <li class="nav-item dropdown <?php echo (current_url() == base_url() . 'Mahasiswa/tampilnilaimhs') ? 'active' : '' ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"> <i class="fas fa-graduation-cap"></i>
                    <span>Data Nilai</span></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo (current_url() == base_url() . 'Mahasiswa/tampilnilaimhs') ? 'active' : '' ?>">
                        <a class="nav-link" href="<?php echo base_url('Mahasiswa/tampilnilaimhs') ?>">Lihat Nilai</a></li>
                </ul>
            </li>
        </ul>

    </aside>
</div>