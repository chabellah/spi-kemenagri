<?php
$this->load->view('templates/dashboard_header');
?>

<?php
$this->load->view('admin/templates/admin_sidebar');
?>


<!-- Start modal tambah dosen -->


<div class="modal fade" tabindex="-1" role="dialog" id="tambah_Modal_Mhs">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Dosen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form method="POST" action="<?php echo base_url('Admin/simpanMhs') ?>">

                <div class="modal-body">
                    <!-- <div class="form-group">
                        <label>Kode Dosen</label>
                        <input type="hidden" name="kd_dosen" id="kd_dosen">

                        <input type="text" class="form-control" name="id_dosen" id="id_dosen">
                    </div> -->
                    <div class="form-group">
                        <label>Nim</label>
                        <input type="text" class="form-control" name="nim" id="nim">
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama">
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" class="form-control" name="alamat" id="alamat">
                    </div>
                    <div class="form-group">
                        <label>No Telpon</label>
                        <input type="text" class="form-control" name="no_telp" id="no_telp">
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <input type="text" class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                    </div>
                    <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir">
                    </div>
                    <div class="form-group">
                        <label>Kewarganegaraan</label>
                        <input type="text" class="form-control" name="kewarganegaraan" id="kewarganegaraan">
                    </div>
                    <div class="form-group">
                        <label>Agama</label>
                        <input type="text" class="form-control" name="agama" id="agama">
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" id="email">
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" name="username" id="username">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="text" class="form-control" name="password" id="password">
                    </div>
                    <div class="form-group">
                        <label>Role ID</label>
                        <input type="text" class="form-control" name="role_id" id="role_id">
                    </div>


                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>

                </div>
            </form>
        </div>
    </div>
</div>


<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/stisla.js"></script>

<!-- JS Libraies -->
<script src="<?php echo base_url('node_modules') ?>/prismjs/prism.js"></script>

<!-- Template JS File -->
<script src="<?php echo base_url('assets') ?>/js/scripts.js"></script>
<script src="<?php echo base_url('assets') ?>/js/custom.js"></script>

<!-- Page Specific JS File -->
<script src="<?php echo base_url('assets') ?>/js/page/bootstrap-modal.js"></script>